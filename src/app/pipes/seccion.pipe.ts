import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'seccion'
})
export class SeccionPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
     
    switch(value){
      case '1' : return 'Ingenieria de Minas, geologia y civil ';
      case '2': return 'Biologia';
    }
  }

}

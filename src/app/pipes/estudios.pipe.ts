import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'estudios'
})
export class EstudiosPipe implements PipeTransform {

  transform(value: any): any {
     
    switch(value){
      case 1 : return 'Maestría';
      case 2: return 'Doctorado';
    }
  }

}

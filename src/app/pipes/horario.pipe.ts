import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'horario'
})
export class HorarioPipe implements PipeTransform {

  transform(value: any): any {
     
    switch(value){
      case 0 : return '09:00';
      case 1 : return '09:10';
      case 2 : return '09:20';
      case 3 : return '09:30';
      case 4 : return '09:40';
      case 5 : return '09:50';
      case 6 : return '10:00';
      case 7 : return '10:10';
      case 8 : return '10:20';
      case 9 : return '10:30';
      case 10 : return '10:40';
      case 11: return '10:50';
      case 12: return '11:00';
      case 13: return '11:10';
      case 14: return '11:20';
      case 15: return '11:30';
      case 16: return '11:40';
      case 17: return '11:50';
      case 18: return '12:00';
      case 19: return '12:10';
      case 20: return '12:20';
      case 21: return '12:30';
      case 22: return '12:40';
      case 23: return '12:50';      
    }
  }

}

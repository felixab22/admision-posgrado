import { NgModule } from '@angular/core';
import { SeccionPipe } from './seccion.pipe';
import { EstudiosPipe } from './estudios.pipe';
import { UniversidadPipe } from './universidad.pipe';
import { HorarioPipe } from './horario.pipe';
import { MencionPipe } from './mencion.pipe';



@NgModule({
  declarations: [
    SeccionPipe,
    EstudiosPipe,
    UniversidadPipe,
    HorarioPipe,
    MencionPipe
  ],
  imports: [
    
  ], 
  exports :[
    SeccionPipe,
    EstudiosPipe,
    UniversidadPipe,
    HorarioPipe,
    MencionPipe
  ]
})
export class PipesModule { }

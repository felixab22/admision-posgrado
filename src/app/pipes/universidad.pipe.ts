import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'universidad'
})
export class UniversidadPipe implements PipeTransform {

  transform(value: any): any {
     
    switch(value){
      case "1" : return 'PÚBLICA';
      case "2": return 'PRIVADA';
    }
  }

}

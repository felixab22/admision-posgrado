import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mencion'
})
export class MencionPipe implements PipeTransform {

  transform(value: any): any {
     
    switch(value){
      case 1 : return 'AGRONEGOCIOS';
      case 2: return 'MANEJO DE CUENCAS HIDROGRAFICAS';
      case 3: return 'SALUD Y PRODUCCION ANIMAL';
      case 4: return 'PRODUCCION AGRICOLA SOSTENIBLE';
      case 5: return 'MICROBIOLOGIA';
      case 6: return 'SANEAMIENTO ALIMENTARIO Y AMBIENTAL';
      case 7: return 'GESTION AMBIENTAL Y BIODIVERSIDAD';
      case 8: return 'DOCTORADO EN EDUCACION';
      case 9: return 'DOCENCIA UNIVERSITARIA';
      case 10: return 'ESTRATEGIAS DE ENSEÑANZA - APRENDIZAJE Y EVALUACION';
      case 11: return 'EDUCACION INTERCULTURAL BILINGUE';
      case 12: return 'GESTION EDUCACIONAL';
      case 13: return 'ANTROPLOLOGIA';
      case 14: return 'AUDITORIA INTEGRAL';
      case 15: return 'GESTION EMPRESARIAL';
      case 16: return 'GERENCIA SOCIAL';
      case 17: return 'GESTION PUBLICA';
    //   case 18: return 'CIENCIAS PENALES';
    //   case 19: return 'DERECHO CIVIL Y COMERCIAL';
      case 20: return 'CIENCIAS PENALES';
      case 21: return 'DERECHO CIVIL Y COMERCIAL';
      case 22: return 'EPIDEMIOLOGIA';
      case 23: return 'GERENCIA EN SERVICIOS DE SALUD';
      case 24: return 'ATENCION FARMACEUTICA Y FARMACIA CLINICA';
      case 25: return 'SALUD PUBLICA';
      case 26: return 'GERENCIA DE PROYECTOS Y MEDIO AMBIENTE';
      case 27: return 'INGENIERIA AMBIENTAL';
    }
  }

}

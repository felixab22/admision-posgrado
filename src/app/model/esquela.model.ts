export class PersonaModel {
    idpersona: number;
    nombre: string;
    apellido: string;
    dni: string;
    telefono: string;
    correo: string;
}
export class OperacionModel {
    dni: string;
    numoperacion: string;
}
export class EsquelaModel {
    idesquela: number;
    sede :string;
    nroesquela?: string;
    tipoestudio: number; // 1 === maestría , 2 === doctorado
    femision: Date;
    idpostulante: any;
    idmencion: any;
    modalidad: string;
    monto: number;
}

export class BoucherModel {
    idboucher: number;
    nrooperacion: string;
    monto: number;
    fecha: Date;
    postulante: string;
    dni: number;
}
export class FichaModel {
    idficha: number;
    // sede: string;
    tipouniversidad: string;
    nombreuniv: string;
    direcuniv: string;
    fgradoobt: string;
    vecespost: string;
    finscripcion: Date; //backend
    idesquela: any;
    idboucher: any;
    direccion: string;
    codigo?: any;
}
/* cSpell:disable */
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { BienvenidosComponent } from './pages/bienvenidos/bienvenidos.component';

const routes: Routes = [
  {
    path: '',
    component: BienvenidosComponent
  },
  {
    path: 'Posgrado-unsch',
    component: PagesComponent,
    children:
      [
        {
          path: 'Inscripcion',
          loadChildren: './pages/inscripcion/inscripcion.module#InscripcionModule'
        },
        {
          path: 'Concurso',
          loadChildren: './pages/concurso/concurso.module#ConcursoModule'
        },
        {
          path: 'Reportes',
          loadChildren: './pages/reportes/reportes.module#ReportesModule'
        }
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

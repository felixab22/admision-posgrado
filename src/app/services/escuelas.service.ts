import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URL_SERVICIOS } from '../pages/config/confing';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EscuelasService {
  api = URL_SERVICIOS;
  public escuela = [
    { value: 1, label: 'Facultad de Ciencias Agrarias' },
    { value: 2, label: 'Facultad de Ciencias Biológicas' },
    { value: 3, label: 'Facultad de Ciencias de la Educación' },
    { value: 4, label: 'Facultad de Ciencias Sociales' },
    { value: 5, label: 'Facultad de Ciencias Económicas, Administrativas y Contables' },
    { value: 6, label: 'Facultad de Derecho y Ciencias Política' },
    { value: 7, label: 'Facultad de Ciencias de la Salud' },
    { value: 8, label: 'Facultad de Ingeniería de Minas, Geología y Civil' },
    { value: 9, label: 'Facultad de Ingeniería Química y Metalurgia' },
  ]
  public escuela2 = [
    { value: 3, label: 'Facultad de Ciencias de la Educación' },
    { value: 5, label: 'Facultad de Ciencias Económicas, Administrativas y Contables' }
  ]
  constructor(
    private http: HttpClient
  ) {

  }
  public getMencionXMaestria(maestria): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/maestria/getMencionByFacultad?idmaestria=${maestria}`);
  }
  public getAllMencion(): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/maestria/getAllMaestria`);
  }
  public getEsquelaXMencion(sede: any, mencion: any): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/esquela/getAllEsquelasByMencion?sede=${sede}&idmencion=${mencion}`);
  }


  public saveOrUpdatePostulante(postulante: any): Observable<any> {
    return this.http.post<any>(this.api + `/postulante/saveOrUpdatePostulante`, JSON.stringify(postulante));
  }

  public getEsquelaXDni(dni): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/esquela/getEsquelaPostulanteByDni?dni=${dni}`);
  }

  public saveOrUpdateEsquela(postulante: any): Observable<any> {
    return this.http.post<any>(this.api + `/esquela/saveOrUpdateEsquela`, JSON.stringify(postulante));
  }
  public saveOrUpdateFicha(ficha: any): Observable<any> {
    return this.http.post<any>(this.api + `/inscripcion/saveOrUpdateFicha`, JSON.stringify(ficha));
  }

  public getEsquelaXOperación(dni, nrooperacion): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/inscripcion/validarPago?nrooperacion=${nrooperacion}&dni=${dni}`);
  }
  public getEsquelaXFicha(dni, nrooperacion): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/inscripcion/getFichaInscripcion?nrooperacion=${nrooperacion}&dni=${dni}`);
  }

  // reportes
  public getReporteAptos(lista, sede,idmencion): Observable<any[]> {
    return this.http.get<any[]>(this.api + `/inscripcion/getReportesPostulantes?lista=${lista}&sede=${sede}&idmencion=${idmencion}`);
  }
}

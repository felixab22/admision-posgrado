import { Component, OnInit } from '@angular/core';
import { EscuelasService } from '../../services/escuelas.service';
import { Router } from '@angular/router';
import { BoucherModel, OperacionModel } from 'src/app/model/esquela.model';
declare var swal: any;
@Component({
  selector: 'app-bienvenidos',
  templateUrl: './bienvenidos.component.html',
  styleUrls: ['./bienvenidos.component.scss']
})
export class BienvenidosComponent implements OnInit {
  dni: string;
  pago: BoucherModel;
  buscar1: OperacionModel;
  buscar2: OperacionModel;

  constructor(
    private _esquelaSrv: EscuelasService,
    private _router: Router
  ) {
    this.pago = new BoucherModel();
    this.buscar1 = new OperacionModel();
    this.buscar2 = new OperacionModel();
  }

  ngOnInit() {
    this.buscar1.numoperacion = 'I-'
    this.buscar2.numoperacion = 'I-'
  }
  recuperarOperacion(basicModal4) {
    this._esquelaSrv.getEsquelaXFicha(this.buscar2.dni, this.buscar2.numoperacion).subscribe((res: any) => {
      //console.log(res);
      if (res.code === 200) {
        this.pago = res.data;
        basicModal4.hide();
        localStorage.setItem('ficha', JSON.stringify(res.data));
        this._router.navigate(["/Posgrado-unsch/Concurso/Ficha"]);
      }
      else {
        this.buscar1 = new OperacionModel();
        basicModal4.hide();
        swal('NO EXISTE FICHA!', 'Usted debe inscribirse primero haciendo click en el boton INSCRIPCIÓN', 'warning');
      }
    });
  }
  buscarDni() {
    if (this.dni.length > 7) {
      //console.log(this.dni);

      this._esquelaSrv.getEsquelaXDni(this.dni).subscribe((res: any) => {
        //console.log(res);
        if (res.code === 200) {
          localStorage.setItem('buscado', JSON.stringify(res.data));
          this._router.navigate(["/Posgrado-unsch/Inscripcion/Imprimir"]);
        }
        else {
          swal('INCORRECTO!', 'REGISTRO NO ENCONTRADO', 'warning');
        }
      });
    }
  }
  buscarOperacion(basicModal2, basicModal3) {
    this._esquelaSrv.getEsquelaXOperación(this.buscar1.dni, this.buscar1.numoperacion).subscribe((res: any) => {
      //console.log(res);
      if (res.code === 200) {
        this.pago = res.data;
        basicModal2.hide();
        basicModal3.show();
        localStorage.setItem('buscar', JSON.stringify(this.buscar1));
        localStorage.setItem('boucher', JSON.stringify(res.data));
        localStorage.setItem('resultado', JSON.stringify(res.result));
      } else if (res.code === 204) {
        swal('PAGO SIN REGISTRAR!', 'Vuelva a intentar más tarde, recuerde que esta operación se realiza despues de 24 horas realizado su pago', 'warning');
      } else if (res.code === 401) {
        basicModal2.hide();
        swal('BOUCHER REGISTRADO!', 'Ya se generó la ficha de inscripción con este boucher', 'warning');
      }
    });
  }
  ingresar() {
    this._router.navigate(["/Posgrado-unsch/Concurso/Personales"]);
  }
  cancelado(basicModal2, basicModal3) {
    this.buscar1 = new OperacionModel();
    basicModal3.hide();
    basicModal2.show();
  }
  verReportes() {
    //console.log('resportes');

  }
}

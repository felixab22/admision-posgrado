import { CommonModule } from '@angular/common';
import { InscripcionRoutingModule } from './inscripcion-router.module';
import { InscripcionComponent } from './inicio/inscripcion.component';


import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { InscripcionImprimirComponent } from './inscripcion-imprimir/inscripcion-imprimir.component';
import { ModalidadComponent } from './modalidad/modalidad.component';



@NgModule({
  declarations: [InscripcionComponent, InscripcionImprimirComponent, ModalidadComponent],
  imports: [
    CommonModule,
    InscripcionRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    PipesModule
  ],
  providers: [MDBSpinningPreloader],
  schemas: [NO_ERRORS_SCHEMA]
})
export class InscripcionModule { }

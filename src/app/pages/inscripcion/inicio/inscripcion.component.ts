import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { EscuelasService } from '../../../services/escuelas.service';
import { PersonaModel } from 'src/app/model/esquela.model';
import { fromEvent } from 'rxjs';
declare var swal: any;
@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  styleUrls: ['./inscripcion.component.scss']
})
export class InscripcionComponent implements OnInit {
  validatingForm: FormGroup;
  private emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  sexos = [
    { value: 'FEMENINO', label: 'Femenino' },
    { value: 'MASCULINO', label: 'Masculino' }
  ]
  constructor(
    private _router: Router,
    public _esquelaSrv: EscuelasService
  ) {
  }

  ngOnInit() {
    this.validatingForm = new FormGroup({
      apellido: new FormControl('', Validators.required),
      nombre: new FormControl('', Validators.required),
      sexo: new FormControl('', Validators.required),
      dni: new FormControl('', [Validators.minLength(8), Validators.maxLength(8), Validators.required]),
      telefono: new FormControl('', [Validators.minLength(9), Validators.maxLength(9), Validators.required]),
      correo: new FormControl('@gmail.com', [Validators.required, Validators.pattern(this.emailPattern)])
    });
  }
  crearInscripcion(entrada: PersonaModel) {
    entrada.apellido = entrada.apellido.toUpperCase();
    entrada.nombre = entrada.nombre.toUpperCase();
    this._esquelaSrv.saveOrUpdatePostulante(entrada).subscribe((res: any) => {
      console.log(res);
      if (res.code === 200) {
        localStorage.setItem('persona', JSON.stringify(res.data));
        this._router.navigate(["/Posgrado-unsch/Inscripcion/Modalidad"]);
      } if (res.code === 403) {
        swal('DATOS REGISTRADOS!', 'El DNI ya se encuentra registrado, ahora seleccione su carrera', 'warning');
        localStorage.setItem('persona', JSON.stringify(res.data));
        
         this._router.navigate(["/Posgrado-unsch/Inscripcion/Modalidad"]);
      }
    });
  }
  get apellido() {
    return this.validatingForm.get('apellido');
  }
  get sexo() {
    return this.validatingForm.get('sexo');
  }
  get maternoform() {
    return this.validatingForm.get('maternoform');
  }
  get nombre() {
    return this.validatingForm.get('nombre');
  }
  get dni() {
    return this.validatingForm.get('dni');
  }

  get telefono() {
    return this.validatingForm.get('telefono');
  }
  get correo() {
    return this.validatingForm.get('correo');
  }
  validarCorreo(e: KeyboardEvent){
    // //console.log(e.target['value']);
  }

}

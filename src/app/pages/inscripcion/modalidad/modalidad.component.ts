import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EscuelasService } from 'src/app/services/escuelas.service';
import { EsquelaModel, PersonaModel } from '../../../model/esquela.model';
declare var swal: any;

@Component({
  selector: 'app-modalidad',
  templateUrl: './modalidad.component.html',
  styleUrls: ['./modalidad.component.scss']
})
export class ModalidadComponent implements OnInit {
  maestrias: any[] = [];
  menciones: any[] = [];
  facultades: any[] = [];
  esquela: EsquelaModel;
  montoapagar = 0;
  sedes = [
    { value: 'Ayacucho', label: 'Ayacucho' },
    { value: 'Pichari', label: 'Pichari' }
  ]
  estudios = [
    { value: 1, label: 'Maestría' },
    { value: 2, label: 'Doctorado' }
  ];
  modalidades = [
    { value: 1, label: 'Admisión ordinaria' },
    { value: 2, label: 'Traslado interno' },
    { value: 3, label: 'Traslado externo nacional' },
    { value: 4, label: 'Traslado externo internacional' }
  ];
  ocultar = true;
  donde = 'Ayacucho';
  cortar = false;
  persona: PersonaModel;
  ;

  constructor(
    private _router: Router,
    public _esquelaSrv: EscuelasService
  ) {
    this.esquela = new EsquelaModel();
  }

  ngOnInit() {
    this.persona = JSON.parse(localStorage.getItem('persona'));
    this.esquela.idpostulante = JSON.parse(localStorage.getItem('persona')).idpersona;
    this.esquela.nroesquela = 202010400 + this.esquela.idpostulante;
  }

  Cancelar() {
    location.reload();
  }
  seleccionSede(value: string) {
    this.esquela.sede = value;
    this.donde = value;

    if (value === 'Pichari') {
      this.estudios = [
        { value: 1, label: 'Maestría' },
      ];
    }
  }
  selectFacultad(idfacultad) {
    // //console.log(idfacultad);

    switch (idfacultad) {
      case '1': {
        this.maestrias = [
          { value: 1, label: 'Maestría en Agronegocios' },
          { value: 2, label: 'Maestría en Ciencias Agropecuarias' },
        ]
        break
      }
      case '2': {
        this.maestrias = [
          { value: 3, label: 'Maestría en Ciencias' }
        ]
        break
      }
      case '3': {
        if (this.donde === 'Pichari') {
          this.maestrias = [
            { value: 5, label: 'Maestría en Educación' }
          ]
        } else {
          this.maestrias = [
            { value: 4, label: 'Maestría en Docencia Universitaria' },
            { value: 5, label: 'Maestría en Educación' },
          ]
        }

        break
      }
      case '4': {
        this.maestrias = [
          { value: 6, label: 'Maestría en Ciencias Sociales' },
        ]
        break
      }
      case '5': {


        if (this.donde === 'Pichari') {
          this.maestrias = [
            { value: 8, label: 'Maestría en Ciencias Económicas' }
          ]
        } else {
          this.maestrias = [
            { value: 7, label: 'Maestría en Auditoría' },
            { value: 8, label: 'Maestría en Ciencias Económicas' },
          ]
        }
        break
      }
      case '6': {
        this.maestrias = [
          { value: 9, label: 'Maestría en Derecho' },

        ]
        break
      }
      case '7': {
        this.maestrias = [
          { value: 10, label: 'Maestría en Epidemiología' },
          { value: 11, label: 'Maestría en Gerencia en Servicios de Salud' },
          { value: 12, label: 'Maestría en Atención Farmacéutica y Farmacia Clínica' },
          { value: 13, label: 'Maestría en Salud Pública' }

        ]
        break
      }
      case '8': {
        this.maestrias = [
          { value: 14, label: 'Maestría en Ciencias de la Ingeniería' },
        ]
        break
      }
      case '9': {
        this.maestrias = [
          { value: 15, label: 'Maestría en Ingeniería Ambiental' },
        ]
        break
      }
    }
  }


  selectmaestria(maestria) {
    // //console.log(maestria);
    this._esquelaSrv.getMencionXMaestria(maestria).subscribe((res: any) => {
      // //console.log(res);
      this.menciones = res.data;
      if (maestria === '5' && this.donde === 'Pichari') {
        this.menciones = [
          { idmencion: 10, denominacion: "ESTRATEGIAS DE ENSEÑANZA - APRENDIZAJE Y EVALUACION" },
          { idmencion: 12, denominacion: "GESTION EDUCACIONAL" }
        ]
        // //console.log('borrar');

      }
      if (maestria === '8' && this.donde === 'Pichari') {
        this.menciones.shift();
        this.menciones.shift();
      }
      if (maestria === '5' && this.donde === 'Ayacucho') {
        this.menciones.pop();
      }
    });
  }
  salirPagina() {
    swal({
      title: "DESEA SALIR?",
      text: "confirma que ya terminaste de inscribirte!",
      icon: "warning",
      buttons: ["Quedarse!", "Salir!"],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          swal("Salir", {
            icon: 'danger',
          },
          localStorage.clear(),
          this._router.navigate(['/'])

          );
        } else {
          swal("Quedarse!");
        }
      });
  }
  selectestudios(estudios) {
    if (this.donde === 'Ayacucho') {
      this.facultades = this._esquelaSrv.escuela;
    } else {
      this.facultades = this._esquelaSrv.escuela2;
    }
    if (estudios === '1') {
      this.ocultar = true;

    } else {
      this.ocultar = false;
      this.menciones = [
        { idmencion: 8, denominacion: 'Doctorado en Educación' }
      ];
      this.esquela.idmencion = 8;
    }
    this.esquela.tipoestudio = parseInt(estudios);

  }
  selectmodalidades(modalidades) {
    this.esquela.modalidad = modalidades

  }
  selectmenciones(menciones) {
    // //console.log(menciones);
    this.esquela.idmencion = parseInt(menciones);
    this.montoapagar = this.esquela.monto = this.calcularmonto(this.esquela.modalidad, this.esquela.tipoestudio);

  }
  SabeOrUpdateModalidades() {
    // console.log(this.esquela);
    this._esquelaSrv.saveOrUpdateEsquela(this.esquela).subscribe((res: any) => {
      if (res.code === 200) {
        this.buscarDni();
      }
      if (res.code === 403) {
        swal('ATENCIÓN!', 'Usted ya cuenta una esquela generado', 'warning');
      }
    });
  }

  buscarDni() {
    const persona = JSON.parse(localStorage.getItem('persona'));
    this._esquelaSrv.getEsquelaXDni(persona.dni).subscribe((res: any) => {
      localStorage.setItem('buscado', JSON.stringify(res.data));
      this._router.navigate(["/Posgrado-unsch/Inscripcion/Imprimir"]);
    })
  }

  calcularmonto(mod, est): number {
    if (est === 1) {
      if (mod === 'Admisión ordinaria') {
        return 300;
      }
      if (mod === 'Traslado interno') {
        return 250;
      }
      if (mod === 'Traslado externo nacional') {
        return 300;
      }
      if (mod === 'Traslado externo internacional') {
        return 500;
      }
    }
    if (est === 2) {
      if (mod === 'Admisión ordinaria') {
        return 350;
      }
      if (mod === 'Traslado interno') {
        return 0;
      }
      if (mod === 'Traslado externo nacional') {
        return 350;
      }
      if (mod === 'Traslado externo internacional') {
        return 600;
      }
    }

  }
}

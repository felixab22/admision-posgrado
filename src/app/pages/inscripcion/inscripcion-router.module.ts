import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InscripcionComponent } from './inicio/inscripcion.component';
import { InscripcionImprimirComponent } from './inscripcion-imprimir/inscripcion-imprimir.component';
import { ModalidadComponent } from './modalidad/modalidad.component';

const routes: Routes = [
  {
    path: 'Inicio',
    component: InscripcionComponent
  },  
  {
    path: 'Imprimir',
    component: InscripcionImprimirComponent
  
  },
  {
    path: 'Modalidad',
    component: ModalidadComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InscripcionRoutingModule { }

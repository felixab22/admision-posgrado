import { Component, OnInit } from '@angular/core';
import { FichaModel, OperacionModel } from 'src/app/model/esquela.model';
import { EscuelasService } from 'src/app/services/escuelas.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
declare var swal: any
declare function wizards();

@Component({
  selector: 'app-personales',
  templateUrl: './personales.component.html',
  styleUrls: ['./personales.component.scss']
})
export class PersonalesComponent implements OnInit {
  newFicha: FichaModel;
  aceptadocheckbox: string[] = [];
  mostrar = true;
  buscar2: OperacionModel;
  // sedes = [
  //   { value: 1, label: 'Ayacucho' },
  //   { value: 2, label: 'Pichari' }
  // ]
  universidades = [
    { value: 1, label: 'Pública' },
    { value: 2, label: 'Privada' }
  ]
  veces = [
    { value: 1, label: '1' },
    { value: 2, label: '2' },
    { value: 3, label: '3' },
    { value: 4, label: '4' },
    { value: 5, label: '5' },
    { value: 6, label: '6' },
    { value: 7, label: '7' },
    { value: 8, label: '8' },
    { value: 9, label: '9' },
    { value: 10, label: '10' },
  ]
  boucher: any;
  resultado: any;

  constructor(
    private _router: Router,
    private _esquelaSrv: EscuelasService
  ) {
    this.newFicha = new FichaModel();
    this.boucher = JSON.parse(localStorage.getItem('boucher'));
    this.resultado = JSON.parse(localStorage.getItem('resultado'));
    this.buscar2 = JSON.parse(localStorage.getItem('buscar'));
  }

  ngOnInit() {
    wizards();
  }
  recuperarOperacion() {
    //console.log(this.buscar2);
    this._esquelaSrv.getEsquelaXFicha(this.buscar2.dni, this.buscar2.numoperacion).subscribe((res: any) => {
      //console.log(res);
      if (res.code === 200) {
        localStorage.setItem('ficha', JSON.stringify(res.data));
        this._router.navigate(["/Posgrado-unsch/Concurso/Ficha"]);
      } else {
         swal('Mal!', 'Los campos con * son obligatorios', 'warning');
      }
    });
  }
  seleccionUniversidad(data) {
    this.newFicha.tipouniversidad = data;

  }
  seleccionVecespostula(veces) {
    this.newFicha.vecespost = veces;
  }
  // seleccionSede(value) {
  //   this.newFicha.sede = value;

  // }
  SaveOrUpdate() {
    const valor = localStorage.getItem('valor');
    if (valor === 'true') {
      this.newFicha.nombreuniv = this.newFicha.nombreuniv.toLocaleUpperCase();
      this.newFicha.direcuniv = this.newFicha.direcuniv.toLocaleUpperCase();
      this.newFicha.direccion = this.newFicha.direccion.toLocaleUpperCase();

      this.newFicha.idboucher = this.boucher.idboucher;
      this.newFicha.idesquela = this.resultado.idesquela;

      this._esquelaSrv.saveOrUpdateFicha(this.newFicha).subscribe((res: any) => {
        //console.log(res);
        
        if (res.code === 200) {
          swal('Bien!', 'Guardado!', 'success').then(() => {
            this.recuperarOperacion();
          });
        }
      });
    }
  }
  chenckboxChangeDeclaracion(bienes) {
    if (this.aceptadocheckbox.indexOf(bienes.value) === -1) {
      this.aceptadocheckbox.push(bienes.value);
      //console.log(this.aceptadocheckbox.length);
      this.mostrar = false;

    } else {
      this.aceptadocheckbox.splice(this.aceptadocheckbox.indexOf(bienes.value), 1);
      //console.log(this.aceptadocheckbox.length);
      this.mostrar = true;


    }
  }

}

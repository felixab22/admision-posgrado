import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var swal: any;
@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.scss']
})
export class FichaComponent implements OnInit {
  persona: any = null;
  constructor(
    private _router: Router

  ) {
    this.persona = JSON.parse(localStorage.getItem('ficha'));
    if( this.persona === null){
      this._router.navigate(['/'])}
    
  }

  ngOnInit() {
  }
  salirPagina() {
    swal({
      title: "DESEA SALIR?",
      text: "confirma que ya terminaste de inscribirte!",
      icon: "warning",
      buttons: ["Quedarse!", "Salir!"],
      dangerMode: true
    })
      .then((willDelete) => {
        if (willDelete) {
          swal("Salir", {
            icon: 'danger',
          },
          localStorage.clear(),
          this._router.navigate(['/'])          
          );
        } else {
          swal("Quedarse!");
        }
      });
  }
  createPDF() {
    var sTable = document.getElementById('imprimirficha').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}";
    style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h1,h2, h3, h4 , h5  {text-align: center; padding:5px; margin: 5px; font-family: Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "h6 {font-size: 10px; padding:5px; margin: 5px; }";
    style = style + ".logo img { width: 50%; height: 100%;}";
    style = style + "table {width: 100%;font: 11px Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse; padding-left: 5px;}";
    style = style + "th {text-align: left;width: 130px;}";
    style = style + "td {font-size: 12px;font-weight: 400; font-family: Tahoma, Geneva, Verdana, sans-serif;height: 25px;}";
    style = style + "td.derecha {text-align: right;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write(`<title>Ficha_inscripcion${this.persona.idficha}</title>`);   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
}

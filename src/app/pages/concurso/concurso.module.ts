import { CommonModule } from '@angular/common';
import { ConcursoRoutingModule } from './concurso-router.module';
import { PersonalesComponent } from './personales/personales.component';
import { DatepickerModule, WavesModule } from 'ng-uikit-pro-standard'
import { FormsModule } from '@angular/forms';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { HttpClientModule } from '@angular/common/http';
import { FichaComponent } from './ficha/ficha.component';
import { PipesModule } from 'src/app/pipes/pipes.module';


@NgModule({
  declarations: [PersonalesComponent, FichaComponent],
  imports: [
    ConcursoRoutingModule,
    CommonModule,
    DatepickerModule, WavesModule,
    FormsModule,
    HttpClientModule,
    PipesModule,
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
  ],
  providers: [
    MDBSpinningPreloader
  ],
  schemas: [NO_ERRORS_SCHEMA]

})
export class ConcursoModule { }

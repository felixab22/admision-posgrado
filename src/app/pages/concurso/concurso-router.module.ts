import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalesComponent } from './personales/personales.component';
import { FichaComponent } from './ficha/ficha.component';

const routes: Routes = [
  {
    path: 'Personales',
    component: PersonalesComponent
  },
  {
    path: 'Ficha',
    component: FichaComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConcursoRoutingModule { }

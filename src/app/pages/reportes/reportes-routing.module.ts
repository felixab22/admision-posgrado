import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AptosComponent } from './aptos/aptos.component';
import { GeneralComponent } from './general/general.component';


const routes: Routes = [
  {
    path: 'Aptos',
    component: AptosComponent
  },
  {
    path: 'General',
    component: GeneralComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }

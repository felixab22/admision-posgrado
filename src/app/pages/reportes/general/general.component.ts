import { Component, OnInit,Inject } from '@angular/core';
import { EscuelasService } from 'src/app/services/escuelas.service';
import { DOCUMENT } from '@angular/common';
declare var swal: any;
@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  menciones: any[] = [];
  postulantes: any[] = [];
  sedes = [
    { value: 'Ayacucho', label: 'Ayacucho' },
    { value: 'Pichari', label: 'Pichari' }
  ]
  estudios = [
    { value: 1, label: 'Maestría' },
    { value: 2, label: 'Doctorado' }
  ];
  modalidades = [
    { value: 1, label: 'Admisión ordinaria' },
    { value: 2, label: 'Traslado interno' },
    { value: 3, label: 'Traslado externo nacional' },
    { value: 4, label: 'Traslado externo internacional' }
  ];
  tabla1 = false;
  mencion = 0;
  sede = '';
  mencion1 = 'Mencion'
  mencion2 = 'Mencion'
  facultad1 = 'Facultad'
  facultad2 = 'Facultad'
  mencion3 = 'Mencion'
  facultad3 = 'Mencion'
  salon='P';
  grupo='1';
  constructor(
    public _esquelaSrv: EscuelasService,
    @Inject(DOCUMENT) private _document,
  ) { }

  ngOnInit() {
    this._esquelaSrv.getAllMencion().subscribe((res: any) => {
      this.menciones = res.data;
    })
  }
  seleccionSedeAptos(sede: string) {
    this.sede = sede;
  }
  selectmencionesAptos(aptos: number) {
    //console.log(aptos);
    this.mencion = aptos;
    var buscado = this.buscarMencion(aptos);
    //console.log(buscado);   
    this.mencion1 = buscado.mencion;
    this.facultad1 = buscado.facultad;
  }
  seleccionSedeSalon(sede: string) {
    this.sede = sede;
  }
  selectmencionesSalon(mencion: number) {
    //console.log(mencion);
    this.mencion = mencion;
    var buscado = this.buscarMencion(mencion);
    //console.log(buscado);   
    this.mencion2 = buscado.mencion;
    this.facultad2 = buscado.facultad;
  }
  seleccionSedePromedio(sede: string) {
    this.sede = sede;
  }
  selectmencionesPromedio(aptos: number) {
    //console.log(aptos);
    this.mencion = aptos;
    var buscado = this.buscarMencion(aptos);
    //console.log(buscado);   
    this.mencion3 = buscado.mencion;
    this.facultad3 = buscado.facultad;
  }

  buscar() {
    this.postulantes = []; 
    var lista = 1;
    this._esquelaSrv.getReporteAptos(lista, this.sede, this.mencion).subscribe((res: any) => {
      //console.log(res);
      if (res.code === 200) {
        this.tabla1 = true;
        this.postulantes = res.data;
      }
      if (res.code === 204) {
        this.tabla1 = false;
        this.postulantes = [];
      }
    })
  }

  buscarMencion(buscar) {
    var vuelta = null;
    for (let item of this.menciones) {
      if (item.idmencion === parseInt(buscar)) {
        //console.log('entro');        
        vuelta = item;
      }
    }
    return vuelta;
  }
  colorear(link, acta: any) {
    let selectores: any = this._document.getElementsByClassName('selector');
    for (let ref of selectores) {
      ref.classList.remove('colorVerde');
    }
    link.classList.add('colorVerde');    
    // this.newActa. = acta.estudiante.idestudiante;
  }

  editarNota(editarNota) {
    let selectores: any = this._document.getElementsByClassName('chekeado');
    if (editarNota < 0 || editarNota > 100) {
      swal('Mal!', 'LA NOTA DEBE SER ENTRE 0 Y 20', 'warning');
      return;
    } else {
        //console.log(editarNota);
        
      // this.SaveOrUpdateActas(this.newActa);
    }
  }
  createPDFaptos() {
    var sTable = document.getElementById('imprimiraptos').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}";
    // style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h1,h2, h3, h4 , h5  {text-align: center; padding:5px; margin: 5px; font-family: Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "h6 {font-size: 10px; padding:5px; margin: 5px; }";
    style = style + ".logo img { width: 100%;}";
    style = style + "table {width: 100%;font: 11px Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse; padding-left: 5px;}";
    style = style + "th {text-align: center;font-weight: 700;  background: crimson;}";
    style = style + "td {font-size: 12px;font-weight: 400; font-family: Tahoma, Geneva, Verdana, sans-serif;height: 25px;}";
    style = style + "table .numero {width: 20px; text-align: center;}";
    style = style + "table .dni {width: 120px; text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>lista_promedios</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
  createPDFPromedio() {
    var sTable = document.getElementById('imprimirPromedio').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}";
    // style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h1,h2, h3, h4 , h5  {text-align: center; padding:5px; margin: 5px; font-family: Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "h6 {font-size: 10px; padding:5px; margin: 5px; }";
    style = style + ".logo img { width: 100%;}";
    style = style + "table {width: 100%;font: 11px Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse; padding-left: 5px;}";
    style = style + "th {text-align: center;font-weight: 700;  background: crimson;}";
    style = style + "td {font-size: 12px;font-weight: 400; font-family: Tahoma, Geneva, Verdana, sans-serif;height: 25px;}";
    style = style + "table .numero {width: 20px; text-align: center;}";
    style = style + "table .dni {width: 120px; text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>lista_aptos</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }

  createPDFsalon() {
    var sTable = document.getElementById('imprimirsalon').innerHTML;
    var style = "<style>";
    style = style + "div.container {font-family: Tahoma, Geneva, Verdana, sans-serif; width: 210mm; height: 245mm;}";
    // style = style + ".header {display: grid; grid-template-columns: 15% 75%;}";
    style = style + "h1,h2, h3, h4 , h5  {text-align: center; padding:5px; margin: 5px; font-family: Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "h6 {font-size: 10px; padding:5px; margin: 5px; }";
    style = style + ".logo img { width: 100%;}";
    style = style + "table {width: 100%;font: 11px Tahoma, Geneva, Verdana, sans-serif;}";
    style = style + "table, th, td {border: solid 1px black; border-collapse: collapse; padding-left: 5px;}";
    style = style + "th {text-align: center;font-weight: 700;  background: crimson;}";
    style = style + "td {font-size: 12px;font-weight: 400; font-family: Tahoma, Geneva, Verdana, sans-serif;height: 25px;}";
    style = style + "table .numero {width: 20px;  text-align: center;}";
    style = style + "table .postula {width: 270px;}";
    style = style + "table .dni {width: 80px; text-align: center;}";
    style = style + "</style>";
    // CREATE A WINDOW OBJECT.
    var win = window.open('', '', 'height=700,width=700');
    win.document.write('<html><head>');
    win.document.write('<title>lista_salon</title>');   // <title> FOR PDF HEADER.
    win.document.write(style);          // ADD STYLE INSIDE THE HEAD TAG.
    win.document.write('</head>');
    win.document.write('<body>');
    win.document.write(sTable);         // THE TABLE CONTENTS INSIDE THE BODY TAG.
    win.document.write('</body></html>');
    win.document.close(); 	// CLOSE THE CURRENT WINDOW.
    win.print();    // PRINT THE CONTENTS.
  }
}
